
import demo.Student;
import java.util.LinkedHashMap;
import java.util.List;
import org.springframework.web.client.RestTemplate;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author Hemant Anjana
 */
public class TestClient {
    
    public static final String REST_SERVICE_URI = "http://localhost:9090/MetadataIntegratorWebService";

    @SuppressWarnings("unchecked")
    private static void testWebservice() {
        try {
            System.out.println("Testing WS API-----------");
            RestTemplate restTemplate = new RestTemplate();
            
            System.out.println("value---" + restTemplate.getForObject(REST_SERVICE_URI + "/test/service", Student.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        testWebservice();
    }
}
