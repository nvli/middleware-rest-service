/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mit.ws.security.authentication.provider;

import java.util.HashSet;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

/**
 *
 * @author Madhuri
 */
@Component
public class AuthenticationProviderImpl implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();
        System.out.println("------Authentication provider" + username + password);
        GrantedAuthority authorityAdmin = new SimpleGrantedAuthority("ADMIN");
        GrantedAuthority authorityGuest = new SimpleGrantedAuthority("GUEST");
        HashSet authorities = new HashSet();
        authorities.add(authorityAdmin);
        authorities.add(authorityGuest);

        if (username != null && !username.equalsIgnoreCase("madhu")) {
            System.out.println("caught wron uname");
            throw new BadCredentialsException("Username not found.");
        }

        if (password != null && !password.equals("madhu")) {
            System.out.println("caught wron pwd");
            throw new BadCredentialsException("Wrong password.");
        }
        return new UsernamePasswordAuthenticationToken(username, password, authorities);
    }

    @Override
    public boolean supports(Class<?> obj) {
        return obj.equals(UsernamePasswordAuthenticationToken.class);
    }
}
