package in.gov.nvli.mit.ws.controller;

import demo.Student;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Hemant Anjana
 */
@RestController
@RequestMapping("/test")
public class Test {

    @RequestMapping(value = "/service", method = RequestMethod.GET)
    public ModelAndView test() {
        Student stud = new Student();
        stud.setName("Hemant");
        stud.setAddress("RMZ");
        System.out.println("inside test controller");
        ModelAndView mav = new ModelAndView();
        mav.setViewName("login");
        return mav;
    }

    @RequestMapping(value = "/fail", method = RequestMethod.GET)
    public void fail() {
        System.out.println("failed");
    }
}
